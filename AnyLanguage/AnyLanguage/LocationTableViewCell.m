//
//  LocationTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "LocationTableViewCell.h"
#import "AppUtils.h"

@interface LocationTableViewCell ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;
@end

@implementation LocationTableViewCell

- (void)awakeFromNib {
    _cityNameLabel.text = @"Не указано";
    _countryNameLabel.text = @"Не указано";
//    [_activityIndicator startAnimating];
}

- (void) reloadData {
     [_activityIndicator startAnimating];
    __weak typeof(self) weakSelf = self;
    [AppUtils getLocationWithCompletetion:^(NSDictionary *geolocation) {
        if (geolocation) {
            [UIView animateWithDuration:0.6 animations:^{
                [weakSelf.activityIndicator stopAnimating];
                weakSelf.activityIndicatorView.alpha = 0;
                weakSelf.activityIndicatorView.hidden = YES;
            }];
            _countryNameLabel.text = geolocation[@"country"];
            _cityNameLabel.text = geolocation[@"city"];
        }
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
