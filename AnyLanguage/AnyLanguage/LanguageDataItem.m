//
//  LanguageDataItem.m
//  AnyLanguage
//
//  Created by Алексей Петров on 15.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "LanguageDataItem.h"

@implementation LanguageDataItem
- (instancetype) initWithDictionary:(NSDictionary *)resourseDictionary {
    self =  [super initWithDictionary:resourseDictionary];
    if (self) {
        if (![resourseDictionary[@"object"] isEqualToString:@"language"]) {
            return nil;
        }
        _languageID = resourseDictionary[@"id"];
        _object = resourseDictionary[@"object"];
        _nameOfLanguage = resourseDictionary[@"name"];
    }
    return self;
}
@end
