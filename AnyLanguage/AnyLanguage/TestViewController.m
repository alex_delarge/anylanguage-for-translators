//
//  TestViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 09.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UINavigationItem *generalNavigationItem;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // Do any additional setup after loading the view.
}
- (IBAction)close:(id)sender {
//    [_textField resignFirstResponder];
    [self.view.window endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController resignFirstResponder];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapToClose:(UITapGestureRecognizer *)sender {
    [self.navigationController resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
