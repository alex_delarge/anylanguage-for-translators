//
//  CommentCollectionViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 14.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userPic;
@end
