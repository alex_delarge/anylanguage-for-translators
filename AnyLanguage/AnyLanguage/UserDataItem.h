//
//  UserDataItem.h
//  AnyLanguage
//
//  Created by Алексей Петров on 14.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "DataItem.h"
#import <UIKit/UIKit.h>

@interface UserDataItem : DataItem
@property (strong, nonatomic) NSString *forname;
@property (strong, nonatomic) NSString *lastname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) UIImage *userPicture;
@property (strong, nonatomic) NSArray *languages;
@end
