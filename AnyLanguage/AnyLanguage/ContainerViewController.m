//
//  ContainerViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 18.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "ContainerViewController.h"

@interface ContainerViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation ContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular) {
        CALayer *vLayer = _containerView.layer;
        [vLayer setCornerRadius:5];
        [vLayer setBorderWidth:0];
        [vLayer setMasksToBounds:YES];
        
        CALayer *viewLayer = _containerView.layer;
        viewLayer.masksToBounds = NO;
        viewLayer.shadowOffset = CGSizeMake(-5, 5);
        viewLayer.shadowRadius = 3;
        viewLayer.shadowRadius = 5;
        viewLayer.shadowOpacity = 0.5;
        
    }
}

- (IBAction)tapToClose:(UITapGestureRecognizer *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    __weak typeof(self) weakSelf = self;
//    [UIView animateWithDuration:0.2 animations:^{
//        weakSelf.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.6];
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
