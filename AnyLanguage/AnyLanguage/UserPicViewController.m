//
//  NameViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 04.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "UserPicViewController.h"
#import "CameraViewController.h"
#import "AppCore.h"
#import "AppUtils.h"
#import "UserDataItem.h"

@interface UserPicViewController () <CameraViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userPicImageView;
@end

@implementation UserPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)close:(UIBarButtonItem *)sender {
    SAFE_CODE_BEGIN
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.parentViewController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
    } completion:^(BOOL finished) {
        [weakSelf.navigationController.parentViewController dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kUpdateShadow" object:nil];
    }];

    SAFE_CODE_END
}

- (void) cameraViewController:(CameraViewController *)controller didFinishMakingPhoto:(UIImage *)photo {
    typeof(self) weakSelf = self;
    [UIView animateWithDuration:1.0 animations:^{
        weakSelf.userPicImageView.image = photo;
        [AppCore sharedInstance].user.userPicture = photo;
    }];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    CALayer *imageLayer = _userPicImageView.layer;
    [imageLayer setCornerRadius:_userPicImageView.frame.size.height/2];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [UIView animateWithDuration:0.6 animations:^{
        if ([AppCore sharedInstance].user.userPicture) {
        _userPicImageView.image = [AppCore sharedInstance].user.userPicture;
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapToHideKeyboard:(UITapGestureRecognizer *)sender {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view endEditing:YES];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (self.navigationController == nil) {
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier hasPrefix:@"takePhoto"]) {
    CameraViewController *cameraViewController =  (CameraViewController *)[segue.destinationViewController childViewControllers][0];
    cameraViewController.delegate = self;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
