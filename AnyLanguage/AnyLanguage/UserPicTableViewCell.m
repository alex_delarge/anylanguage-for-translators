//
//  UserPicTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "UserPicTableViewCell.h"
#import "AppCore.h"
#import "UserDataItem.h"

@interface UserPicTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *userPicImageView;
@end

@implementation UserPicTableViewCell

- (void)awakeFromNib {
    CALayer *imageLayer = _userPicImageView.layer;
    [imageLayer setCornerRadius:_userPicImageView.frame.size.height/2];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
}

- (void) reloadData {
    if ([AppCore sharedInstance].user.userPicture) {
        _userPicImageView.image = [AppCore sharedInstance].user.userPicture;
    }
}

@end
