//
//  EditProfileViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 01.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "EditProfileViewController.h"
#import "AppCore.h"
#import "AppUtils.h"
#import "LanguageDataItem.h"
#import "UserDataItem.h"
#import "RegistrationHeaderView.h"
#import "UserImageTableViewCell.h"
#import "EmailAndNameTableViewCell.h"
#import "LocationTableViewCell.h"
#import "LanguagesTableViewCell.h"
#import "AddTableViewCell.h"
#import "CameraViewController.h"
#import "EmailAndNameViewController.h"
#import "LangugesViewController.h"
#import "EditHeaderView.h"
#import "NameTableViewCell.h"
#import "BDataTableViewCell.h"
#import "GenderTableViewCell.h"
#import "EditContactsTableViewCell.h"

static const NSInteger kSectionWithUserImage = 0; // with user pic
static const NSInteger kSectionWithUserInfo = 1;
static const NSInteger kSectionWithUserContacts = 2;
static const NSInteger kSectionWithUserLocation = 3;
static const NSInteger kSectionWithUserLanguages = 4;


@interface EditProfileViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *sourseArray;
@property (weak, nonatomic) IBOutlet UILabel *numberOfInfo;
@property (weak, nonatomic) IBOutlet UIProgressView *infoProgress;
@property (strong, nonatomic) UIStoryboard *registrationStoryboadrd;
@property (strong, nonatomic) EditHeaderView *emailAndNameHeaderView;
@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _infoProgress.progress = 0;
    _registrationStoryboadrd = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:[NSBundle mainBundle]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica-Light" size:17.0], NSFontAttributeName,nil]];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 44;
    _sourseArray = @[].copy;
//    EditContactsTableViewCell
     [_tableView registerNib:[UINib nibWithNibName:@"EditContactsTableViewCell" bundle:nil] forCellReuseIdentifier:@"EditContactsTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"GenderTableViewCell" bundle:nil] forCellReuseIdentifier:@"GenderTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"BDataTableViewCell" bundle:nil] forCellReuseIdentifier:@"BDataTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"UserImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserImageTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"NameTableViewCell" bundle:nil] forCellReuseIdentifier:@"NameTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"LocationTableViewCell" bundle:nil] forCellReuseIdentifier:@"LocationTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"LanguagesTableViewCell" bundle:nil] forCellReuseIdentifier:@"LanguagesTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"AddTableViewCell" bundle:nil] forCellReuseIdentifier:@"AddTableViewCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.view endEditing:YES];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    EditHeaderView *editHeaderView = [[EditHeaderView alloc] init];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditHeaderView" owner:self options:nil];
    editHeaderView = (EditHeaderView *)[nib objectAtIndex:0];
    switch (section) {
        case kSectionWithUserImage: {
            editHeaderView.textLabel.text = @"Изображение";
            editHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
            if ([AppCore sharedInstance].user.userPicture) {
                editHeaderView.statusImageView.image = [UIImage imageNamed:@"succes button"];
            }
            return editHeaderView;
            break;
        }
        case kSectionWithUserInfo: {
            editHeaderView.textLabel.text = @"Данные";
            editHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
            if ([AppCore sharedInstance].user.lastname && [AppCore sharedInstance].user.forname && [AppCore sharedInstance].user.username) {
                if (![[AppCore sharedInstance].user.lastname isEqualToString:@""] && ![[AppCore sharedInstance].user.forname isEqualToString:@""] && ![[AppCore sharedInstance].user.username isEqualToString:@""]) {
                      editHeaderView.statusImageView.image = [UIImage imageNamed:@"succes button"];
                    
                }
            }
            _emailAndNameHeaderView = editHeaderView;
            return editHeaderView;
            break;
        }
        case kSectionWithUserLocation: {
            editHeaderView.textLabel.text = @"Местоположение";
            editHeaderView.statusImageView.image = [UIImage imageNamed:@"succes button"];
            return editHeaderView;
            break;
        }
        case kSectionWithUserLanguages: {
//            [registrationHeaderView configHeaderWithText:@"Языки"];
              editHeaderView.textLabel.text = @"Языки";
            editHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
            if ([AppCore sharedInstance].user.languages.count > 0) {
                editHeaderView.statusImageView.image = [UIImage imageNamed:@"succes button"];
            }
            return editHeaderView;
        }
        case kSectionWithUserContacts: {
            editHeaderView.textLabel.text = @"Контакты";
            editHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
            return editHeaderView;

        }
        default:
            return nil;
            break;
    }
    return nil;
}


-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _sourseArray = [AppCore sharedInstance].user.languages;
    [_tableView reloadData];
}

- (void) updateInfoProgress {
    NSInteger counter = 0;
    _emailAndNameHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
    if ([AppCore sharedInstance].user.userPicture) {
        counter++;
    }
    
    if ([AppCore sharedInstance].user.lastname && [AppCore sharedInstance].user.forname && [AppCore sharedInstance].user.username) {
        if (![[AppCore sharedInstance].user.lastname isEqualToString:@""] && ![[AppCore sharedInstance].user.forname isEqualToString:@""] && ![[AppCore sharedInstance].user.username isEqualToString:@""]) {
            _emailAndNameHeaderView.statusImageView.image = [UIImage imageNamed:@"succes button"];
            counter ++;
        }
        else {
            _emailAndNameHeaderView.statusImageView.image = [UIImage imageNamed:@"abort button"];
        }
    }
    if ([AppCore sharedInstance].user.languages.count > 0) {
         counter++;
    }
    [ _infoProgress setProgress:(counter / 6.0) animated:YES];
    _numberOfInfo.text = [NSString stringWithFormat:@"%li/5", counter];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateInfoProgress];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == kSectionWithUserImage) {
//        return CGFLOAT_MIN;
//    }
    return 40;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == kSectionWithUserImage) {
            return 2;
    }
    if (section == kSectionWithUserLanguages) {
        return _sourseArray.count + 1;
    }
    if (section == kSectionWithUserInfo) {
        return 3;
    }
    if (section == kSectionWithUserLocation) {
        return 2;
    }
    if (section == kSectionWithUserContacts) {
        return 1;
    }
    return 2;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSectionWithUserImage) {
            if (indexPath.row == 0) {
                UserImageTableViewCell *userPicTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UserImageTableViewCell" forIndexPath:indexPath];
                [userPicTableViewCell reloadData];
                return userPicTableViewCell;
            }
            else if (indexPath.row == 1) {
                AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
                [addTAbleViewCell.addButton addTarget:self action:@selector(changeUserPicture) forControlEvents:UIControlEventTouchUpInside];
                [addTAbleViewCell.addButton setTitle:@"Изменить" forState:UIControlStateNormal];
                return addTAbleViewCell;
            }
    }
    
    if (indexPath.section == kSectionWithUserContacts) {
        EditContactsTableViewCell *editContactsTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"EditContactsTableViewCell" forIndexPath:indexPath];
        return editContactsTableViewCell;
    }
    if (indexPath.section == kSectionWithUserInfo) {
        if (indexPath.row == 0) {
            NameTableViewCell *emailAndNameTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"NameTableViewCell" forIndexPath:indexPath];
            [emailAndNameTableViewCell.changeButton addTarget:self action:@selector(changeUserData) forControlEvents:UIControlEventTouchUpInside];
            emailAndNameTableViewCell.lastnameTexField.delegate = self;
            emailAndNameTableViewCell.fornameText.delegate = self;
            emailAndNameTableViewCell.usernameText.delegate = self;
            return emailAndNameTableViewCell;
        }
        else if (indexPath.row == 1) {
           BDataTableViewCell *bdayCell = [tableView dequeueReusableCellWithIdentifier:@"BDataTableViewCell" forIndexPath:indexPath];
            return bdayCell;
        } else if (indexPath.row == 2) {
            GenderTableViewCell *genderTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"GenderTableViewCell" forIndexPath:indexPath];
            return genderTableViewCell;
        }
    }
    if (indexPath.section == kSectionWithUserLocation) {
        if (indexPath.row == 0) {
            LocationTableViewCell *locationTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"LocationTableViewCell" forIndexPath:indexPath];
            [locationTableViewCell reloadData];
            return locationTableViewCell;
        } else if (indexPath.row == 1) {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [ addTAbleViewCell.addButton setTitle:@"Изменить" forState:UIControlStateNormal];
            [addTAbleViewCell.addButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
            return addTAbleViewCell;
        }
        
    }
    if (indexPath.section == kSectionWithUserLanguages) {
        if (indexPath.row < _sourseArray.count) {
            LanguagesTableViewCell *languagesTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"LanguagesTableViewCell" forIndexPath:indexPath];
            LanguageDataItem *language = _sourseArray[indexPath.row];
            [languagesTableViewCell setNameOfLanguage:language.nameOfLanguage];
            return languagesTableViewCell;
        } else {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [addTAbleViewCell.addButton addTarget:self action:@selector(addNewLanguages) forControlEvents:UIControlEventTouchUpInside];
            [ addTAbleViewCell.addButton setTitle:@"Добавить" forState:UIControlStateNormal];
            return addTAbleViewCell;
        }
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"temporateCell" forIndexPath:indexPath];
    return cell;
}

- (void) addNewLanguages {
    LangugesViewController *languagesViewController = [_registrationStoryboadrd instantiateViewControllerWithIdentifier:@"LangugesViewController"];
    [self.navigationController presentViewController:languagesViewController animated:YES completion:nil];
}

- (void) changeUserLocation {
    UINavigationController *navigationWithSettingUserLocation = [_registrationStoryboadrd instantiateViewControllerWithIdentifier:@"setLocation"];
    navigationWithSettingUserLocation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    navigationWithSettingUserLocation.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:navigationWithSettingUserLocation animated:YES completion:nil];
}

- (void) changeUserPicture {
    UINavigationController *navigationWithCameraViewController = [_registrationStoryboadrd instantiateViewControllerWithIdentifier:@"CameraViewController"];
    navigationWithCameraViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navigationWithCameraViewController animated:YES completion:nil];
}

- (void) changeUserData {
    EmailAndNameViewController *emailAndNameViewController = [_registrationStoryboadrd instantiateViewControllerWithIdentifier:@"EmailAndNameViewController"];
    [self presentViewController:emailAndNameViewController animated:YES completion:nil];
}


- (IBAction)close:(UIBarButtonItem *)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [_tableView setContentInset:edgeInsets];
        [_tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [_tableView setContentInset:edgeInsets];
        [_tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    SAFE_CODE_BEGIN
    if (textField.tag == 1000) {
        [AppCore sharedInstance].user.forname = textField.text;
        [textField resignFirstResponder];
    }
    if (textField.tag == 999) {
        [AppCore sharedInstance].user.lastname = textField.text;
        [textField resignFirstResponder];
    }
    if (textField.tag == 998) {
        [AppCore sharedInstance].user.username = textField.text;
        [textField resignFirstResponder];
    }

    [textField resignFirstResponder];
    [self updateInfoProgress];
    return YES;
    SAFE_CODE_END
    return NO;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.tag == 999) {
        [AppCore sharedInstance].user.forname = textField.text;
    }
    if (textField.tag == 998) {
        [AppCore sharedInstance].user.lastname = textField.text;
    }
    if (textField.tag == 997) {
        [AppCore sharedInstance].user.username = textField.text;
    }
    
    [textField resignFirstResponder];
    [self updateInfoProgress];

    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
#pragma mark - Navigation

// In a storyboard	-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
