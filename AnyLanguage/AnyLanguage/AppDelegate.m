//
//  AppDelegate.m
//  AnyLanguage
//
//  Created by Алексей Петров on 30.07.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "AppDelegate.h"
#import "AppUtils.h"
#import "AppCore.h"
#import "PDKeychainBindings.h"


@interface AppDelegate ()
@property (nonatomic, strong) UITabBarController *rootViewController;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)
launchOptions {
    SAFE_CODE_BEGIN
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *generalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    // set up controller
    _rootViewController = [generalStoryboard instantiateViewControllerWithIdentifier:@"RootTapBarController"];
    _rootViewController.tabBar.tintColor = [UIColor whiteColor]; //[UIColor colorWithRed:255/255 green:0/255 blue:0/255 alpha:1];
    
    
    // set icons to Tab Bar
    for (NSInteger index = 0; index < 3; index++) {
        UIImage *selectedImage = nil;
        UIImage *unselectedImage = nil;
        if (0 == index) {
            selectedImage = [UIImage imageNamed:@"taskIcon"];
            unselectedImage = [UIImage imageNamed:@"taskIconEmpty"];
        }
        else if (1 == index) {
            selectedImage = [UIImage imageNamed:@"profileIcon"];
            unselectedImage = [UIImage imageNamed:@"profileIconEmpty"];
        }
        else if (2 == index) {
            selectedImage = [UIImage imageNamed:@"notificationIcon"];
            unselectedImage = [UIImage imageNamed:@"notificationIconEmpty"];
        }
        UITabBar *tabBar = _rootViewController.tabBar;
        UITabBarItem *tabBarItem = [tabBar.items objectAtIndex:index];
        [tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:12.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
        tabBarItem.selectedImage = selectedImage;
        tabBarItem.image = unselectedImage;
    }
    
    //start tab bar view controller
    self.window.rootViewController = _rootViewController;
   [self.window makeKeyAndVisible];

    //login
    NSString *password =  @""; //[[PDKeychainBindings sharedKeychainBindings] stringForKey:@"password"];
    NSString *email =   @""; //[[PDKeychainBindings sharedKeychainBindings] stringForKey:@"email"];
    NSMutableDictionary *params = @{}.mutableCopy;
    if (!email && !password) {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:[NSBundle mainBundle]];
        UINavigationController *loginViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        [[_rootViewController.viewControllers firstObject] presentViewController:loginViewController animated:YES completion:nil];
        [self.window makeKeyAndVisible];
    }
    [params setObject:email forKey:@"email"];
    [params setObject:password forKey:@"password"];
    [[AppCore sharedInstance] requestAPI:@"auth" inGroup:@"user" withParameters:params succes:^(id response, AFHTTPRequestOperation *operation) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:[NSBundle mainBundle]];
        UINavigationController *loginViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
       [_rootViewController presentViewController:loginViewController animated:YES completion:nil];
        [self.window makeKeyAndVisible];
    }];
    return YES;
    SAFE_CODE_END
    return NO;
}

- (void)resetWindowToInitialView
{
    self.window.rootViewController = self.window.rootViewController.storyboard.instantiateInitialViewController;
    UIStoryboard *generalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    // set up controller
    UITabBarController *rootViewController = [generalStoryboard instantiateViewControllerWithIdentifier:@"RootTapBarController"];
    rootViewController.tabBar.tintColor = [UIColor colorWithRed:255/255 green:0/255 blue:0/255 alpha:1];
    
    
    // set icons to Tab Bar
    for (NSInteger index = 0; index < 3; index++) {
        UIImage *selectedImage = nil;
        UIImage *unselectedImage = nil;
        if (0 == index) {
            selectedImage = [UIImage imageNamed:@"taskIconFull"];
            unselectedImage = [UIImage imageNamed:@"taskIconEmpty"];
        }
        else if (1 == index) {
            selectedImage = [UIImage imageNamed:@"profileIconFull"];
            unselectedImage = [UIImage imageNamed:@"profileIconEmpty"];
        }
        else if (2 == index) {
            selectedImage = [UIImage imageNamed:@"notificationIconFull"];
            unselectedImage = [UIImage imageNamed:@"notificationIconEmpty"];
        }
        UITabBar *tabBar = _rootViewController.tabBar;
        UITabBarItem *tabBarItem = [tabBar.items objectAtIndex:index];
        [tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:12.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
        tabBarItem.selectedImage = selectedImage;
        tabBarItem.image = unselectedImage;
    }
    
    //start tab bar view controller
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
//    int i = 0;
//    [NSNotificationCenter defaultCenter] postNotification:@"kDidChangeNotification "
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

@end
