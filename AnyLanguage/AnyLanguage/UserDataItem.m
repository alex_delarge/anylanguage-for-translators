//
//  UserDataItem.m
//  AnyLanguage
//
//  Created by Алексей Петров on 14.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "UserDataItem.h"

@implementation UserDataItem

- (instancetype) init {
    self = [super init];
    if (self) {
        _languages = @[].copy;
    }
    return self;
}

- (instancetype) initWithDictionary:(NSDictionary *)resourseDictionary {
    self = [super initWithDictionary:resourseDictionary];
    if (self) {
    }
    return self;
}
@end
