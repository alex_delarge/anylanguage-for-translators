//
//  UserImageTableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 03.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userPicImageView;
- (void) reloadData;
@end
