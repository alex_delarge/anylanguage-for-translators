//
//  CardCollectionViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 13.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "CardCollectionViewCell.h"

@interface CardCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIView *backroundView;
@end

@implementation CardCollectionViewCell

- (void)awakeFromNib {
    CALayer *vLayer = _backroundView.layer;
    [vLayer setCornerRadius:15];
    [vLayer setBorderWidth:0.5];
    [vLayer setBorderColor:[[UIColor colorWithRed:41.0/255.0 green:41.0/255.0 blue:41.0/255.0 alpha:255.0/255.0] CGColor]];
    [vLayer setMasksToBounds:YES];
}

@end
