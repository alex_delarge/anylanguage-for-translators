//
//  CameraViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 08.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "CameraViewController.h"
#import "AppUtils.h"
#import "AppCore.h"
#import "UserDataItem.h"

@interface CameraViewController ()
@property (weak, nonatomic) IBOutlet UIVisualEffectView *resultBlurView;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIButton *showGalleryButton;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurEffect;
@property (weak, nonatomic) IBOutlet UIImageView *outputImageView;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *acivityIndicator;
@property (strong, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (strong, nonatomic) NSMutableArray *arrayOfIndexes;
@property (weak, nonatomic) IBOutlet UIView *blurView;

@end

@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    SAFE_CODE_BEGIN
    CALayer *imageLayer = _takePhotoButton.layer;
    [imageLayer setCornerRadius:_takePhotoButton.frame.size.height/2];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor whiteColor] CGColor]];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];

    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    AVCaptureStillImageOutput *newStillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    AVVideoCodecJPEG, AVVideoCodecKey,
                                    nil];
    [newStillImageOutput setOutputSettings:outputSettings];
    if ([session canAddOutput:newStillImageOutput]) {
        [session addOutput:newStillImageOutput];
    }
    self.stillImageOutput = newStillImageOutput;
    session.sessionPreset = AVCaptureSessionPresetMedium;
    CALayer *viewLayer = _outputImageView.layer;
    NSLog(@"viewLayer = %@", viewLayer);
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    captureVideoPreviewLayer.frame = _outputImageView.frame;
    [_outputImageView.layer addSublayer:captureVideoPreviewLayer];
    
    AVCaptureDevice *device = [self frontCamera];
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!input) {
        NSLog(@"ERROR: trying to open camera: %@", error);
    }
    [session addInput:input];
    [session startRunning];
    typeof(self) weakSelf = self;
    [UIView animateWithDuration:1.0
                     animations:^{
                         SAFE_CODE_BEGIN
                         [UIView animateWithDuration:1.0 animations:^{
                             SAFE_CODE_BEGIN
                             weakSelf.showGalleryButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
                               weakSelf.takePhotoButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
                             [weakSelf.acivityIndicator stopAnimating];
                             weakSelf.blurEffect.alpha = 0;
                             [self.navigationController setNavigationBarHidden:NO animated:YES];
                             SAFE_CODE_END
                         }];
                         weakSelf.outputImageView.alpha = 1;
                         SAFE_CODE_END
                     }
                     completion:^(BOOL finished){
                     }];
    SAFE_CODE_END
}

- (AVCaptureDevice *)frontCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront) {
            return device;
        }
    }
    return nil;
}

- (IBAction)close:(UIBarButtonItem *)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)hideResultView:(UIButton *)sender {
    typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.6
                     animations:^{
                         SAFE_CODE_BEGIN
                         weakSelf.resultBlurView.alpha = 0;
                         SAFE_CODE_END
                     }
                     completion:^(BOOL finished){
                         weakSelf.resultBlurView.hidden = YES;
                     }];
    
}

- (IBAction)confirmResult:(UIButton *)sender {
    [AppCore sharedInstance].user.userPicture = _userImage.image;
    [_delegate cameraViewController:self didFinishMakingPhoto:_userImage.image];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takePhoto:(UIButton *)sender {
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in _stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection)
        {
            break;
        }
    }
    typeof(self) weakSelf = self;
    [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         __block UIImage *userPic = [[UIImage alloc] initWithData:imageData];
         
         _resultBlurView.hidden = NO;
       
         [UIView animateWithDuration:0.6
                          animations:^{
                              SAFE_CODE_BEGIN
                              weakSelf.resultBlurView.alpha = 1;
                              weakSelf.userImage.image = userPic;
                              
                              SAFE_CODE_END
                          }
                          completion:^(BOOL finished){
                          }];
         
     }];
}

- (void) dealloc  {
    _userImage = nil;
    _arrayOfImages = nil;
    _arrayOfIndexes = nil;
    _stillImageOutput = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
