//
//  UserInfoTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 21.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "UserInfoTableViewCell.h"
#import "EDStarRating.h"

@interface UserInfoTableViewCell () <EDStarRatingProtocol>
@property (weak, nonatomic) IBOutlet UIImageView *userPictureImageView;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation UserInfoTableViewCell

- (void)awakeFromNib {
    [self.contentView setNeedsLayout];
    CALayer *viewLayer = _userPictureImageView.layer;
    [viewLayer setCornerRadius:_userPictureImageView.frame.size.width/2];
    [viewLayer setBorderWidth:3];
//   [viewLayer setBorderColor:[[UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:255.0/255.0] CGColor]];
    [viewLayer setMasksToBounds:YES];
    [viewLayer setBorderColor:[[UIColor whiteColor] CGColor]];
    _starRating.backgroundColor = [UIColor clearColor];
    _starRating.maxRating = 5.0;
    _starRating.delegate = self;
    _starRating.editable = NO;
    _starRating.tintColor = [UIColor colorWithWhite:0.5 alpha:1];
    _starRating.starImage = [[UIImage imageNamed:@"EDStarRating empty"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRating.starHighlightedImage = [[UIImage imageNamed:@"EDStarRating full"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //    _starRating.horizontalMargin = 15.0;
    _starRating.rating = 2.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
