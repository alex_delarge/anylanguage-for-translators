//
//  RegistrationHeaderView.h
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface RegistrationHeaderView : UIView
- (void) configHeaderWithText:(NSString *) text;
@end
