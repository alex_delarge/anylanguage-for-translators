//
//  TableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 04.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UITextField *fornameText;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTexField;
@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@end
