//
//  FinaceTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 21.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "FinaceTableViewCell.h"
#import "FinanseCollectionViewCell.h"
#import "MoreCollectionViewCell.h"

@interface FinaceTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation FinaceTableViewCell 

- (void)awakeFromNib {
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    //MoreCollectionViewCell
    [_collectionView registerNib:[UINib nibWithNibName:@"MoreCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MoreCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"FinanseCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"FinanseCollectionViewCell"];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 3) {
        FinanseCollectionViewCell *financeCollectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FinanseCollectionViewCell" forIndexPath:indexPath];
        return financeCollectionViewCell;
    }
    MoreCollectionViewCell *moreCollectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MoreCollectionViewCell" forIndexPath:indexPath];
    return moreCollectionViewCell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    if (indexPath.row == 3) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowFinance" object:nil];
    }
}

@end
