//
//  RegistrationHeaderView.m
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "RegistrationHeaderView.h"

@interface RegistrationHeaderView ()
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end

@implementation RegistrationHeaderView

- (void) configHeaderWithText:(NSString *) text {
    if ([text isKindOfClass:([NSString class])]) {
        _headerLabel.text = text;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
