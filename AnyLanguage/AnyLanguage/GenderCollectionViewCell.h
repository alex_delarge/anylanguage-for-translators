//
//  GenderCollectionViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 07.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenderCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *genderName;
@property (weak, nonatomic) IBOutlet UIImageView *confirmImageView;

@end
