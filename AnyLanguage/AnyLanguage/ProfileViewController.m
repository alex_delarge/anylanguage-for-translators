//
//  ProfileViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 21.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "ProfileViewController.h"
#import "UserInfoTableViewCell.h"
#import "RegistrationHeaderView.h"
#import "FinaceTableViewCell.h"
#import "SocialMediaTableViewCell.h"
#import "EducationTableViewCell.h"
#import "EditProfileViewController.h"
#import "EditProfileContainerViewController.h"
#import "HFStretchableTableHeaderView.h"
#import "UserSkilsTableViewCell.h"
#import "ContactTableViewCell.h"
#import "FinanceViewController.h"
#import "ReviewTableViewCell.h"

static const NSInteger kSectionWithUserInfo = 0;
static const NSInteger kSectionWithUserEducation = 1;
static const NSInteger kSectionWithUserReviews = 2;

@interface ProfileViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) HFStretchableTableHeaderView* stretchableTableHeaderView;
@end

@implementation ProfileViewController

- (IBAction)changeUserImage:(UIButton *)sender {
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:[NSBundle mainBundle]];
    UINavigationController *navigationWithCameraViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    navigationWithCameraViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navigationWithCameraViewController animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica-Light" size:17.0], NSFontAttributeName,nil]];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 70;
    //ReviewTableViewCell
    //ShowFinance
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openFinanceDetail) name:@"ShowFinance" object:nil];
    [_tableView registerNib:[UINib nibWithNibName:@"ReviewTableViewCell" bundle:nil] forCellReuseIdentifier:@"ReviewTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"ContactTableViewCell" bundle:nil] forCellReuseIdentifier:@"ContactTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"UserSkilsTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserSkilsTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"UserInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserInfoTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"FinaceTableViewCell" bundle:nil] forCellReuseIdentifier:@"FinaceTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"SocialMediaTableViewCell" bundle:nil] forCellReuseIdentifier:@"SocialMediaTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"EducationTableViewCell" bundle:nil] forCellReuseIdentifier:@"EducationTableViewCell"];
    _stretchableTableHeaderView = [HFStretchableTableHeaderView new];
    [_stretchableTableHeaderView stretchHeaderForTableView:self.tableView withView:_headerView];
}

- (void) openFinanceDetail {
    UIStoryboard *financeStoryboard = [UIStoryboard storyboardWithName:@"FinanceStoryboard" bundle:[NSBundle mainBundle]];
    FinanceViewController *financeViewController = [financeStoryboard instantiateViewControllerWithIdentifier:@"FinanceViewController"];
    [self.navigationController pushViewController:financeViewController animated:YES];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_stretchableTableHeaderView scrollViewDidScroll:scrollView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_stretchableTableHeaderView resizeView];
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == kSectionWithUserInfo) {
        return CGFLOAT_MIN;
    }
    return 40;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == kSectionWithUserInfo) {
        return 6;
    }
    if (section == kSectionWithUserReviews) {
        return 1;
    }
    return 2;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    RegistrationHeaderView *registrationHeaderView = [[RegistrationHeaderView alloc] init];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RegistrationHeaderView" owner:self options:nil];
    registrationHeaderView = (RegistrationHeaderView *)[nib objectAtIndex:0];
    if (section == kSectionWithUserInfo) {
        return nil;
    }
    if (section == kSectionWithUserEducation) {
        [registrationHeaderView configHeaderWithText:@"Образование"];
        return registrationHeaderView;
    }
    
    if (section == kSectionWithUserReviews) {
        [registrationHeaderView configHeaderWithText:@"Отзывы"];
        return registrationHeaderView;
    }
    return nil;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSectionWithUserInfo) {
        if (indexPath.row == 0) {
        UserInfoTableViewCell *userInfiTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UserInfoTableViewCell" forIndexPath:indexPath];
        return userInfiTableViewCell;
        }
        else if (indexPath.row == 1) {
            FinaceTableViewCell *financeTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"FinaceTableViewCell" forIndexPath:indexPath];
            return financeTableViewCell;
        }
        else if (indexPath.row == 4) {
            ContactTableViewCell *contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
            contactTableViewCell.nameLabel.text = @"Электронная почта";
            contactTableViewCell.iconImageView.image = [UIImage imageNamed:@"email icon"];
            return contactTableViewCell;
        }
        else if (indexPath.row == 5) {
            ContactTableViewCell *contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
            contactTableViewCell.nameLabel.text = @"Телефон";
            contactTableViewCell.iconImageView.image = [UIImage imageNamed:@"phone Icon"];
            return contactTableViewCell;
        }

        else if (indexPath.row == 2) {
            UserSkilsTableViewCell *userSkilsTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UserSkilsTableViewCell" forIndexPath:indexPath];
            return userSkilsTableViewCell;
        }
        else if (indexPath.row == 3) {
            UserSkilsTableViewCell *userSkilsTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UserSkilsTableViewCell" forIndexPath:indexPath];
            userSkilsTableViewCell.contentLabel.text = @"Доставка, Верстка";
            return userSkilsTableViewCell;
        }

    }
    
    if (indexPath.section == kSectionWithUserEducation) {
       EducationTableViewCell *educationTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"EducationTableViewCell" forIndexPath:indexPath];
        return educationTableViewCell;
    }
    
    if (indexPath.section == kSectionWithUserReviews) {
        ReviewTableViewCell *reviewTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"ReviewTableViewCell" forIndexPath:indexPath];
        return reviewTableViewCell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"temporateCell" forIndexPath:indexPath];
    return cell;
}

- (IBAction)editProfile:(UIBarButtonItem *)sender {
    UIStoryboard *editStoryboard = [UIStoryboard storyboardWithName:@"EditProfileStoryboard" bundle:[NSBundle mainBundle]];
    EditProfileContainerViewController *editProfileViewController = [editStoryboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    [editProfileViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.tabBarController presentViewController:editProfileViewController animated:YES completion:nil];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowFinance" object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
