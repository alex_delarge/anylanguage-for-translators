//
//  LocationViewController.h
//  AnyLanguage
//
//  Created by Алексей Петров on 11.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RegistrationViewController;

@protocol RegistrationViewControllerDelegate <NSObject>

- (void) didFinishRegistration;

@end

@interface RegistrationViewController : UIViewController
@property (nonatomic, weak) id <RegistrationViewControllerDelegate> delegate;
@end
