//
//  EditHeaderView.h
//  AnyLanguage
//
//  Created by Алексей Петров on 02.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;

@end
