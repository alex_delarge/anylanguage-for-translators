//
//  LanguagesTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 11.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "LanguagesTableViewCell.h"

@interface LanguagesTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;

@end

@implementation LanguagesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) select {
    _selectedImageView.hidden = !self.selected;
}

- (void) setNameOfLanguage:(NSString *) name {
    if (name) {
        _contentLabel.text = name;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    _contentLabel.text = nil;
    _selectedImageView.hidden = YES;
}

@end
