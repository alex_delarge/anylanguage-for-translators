//
//  DataItem.m
//  AnyLanguage
//
//  Created by Алексей Петров on 06.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "DataItem.h"

@implementation DataItem

- (instancetype) initWithDictionary:(NSDictionary *) resourseDictionary {
    self = [super init];
    if (self) {
        if (self) {
            if ([resourseDictionary isKindOfClass:([NSDictionary class])]) {
                return self;
            }
            else {
                return nil;
            }
        }
    }
    return self;
}

@end
