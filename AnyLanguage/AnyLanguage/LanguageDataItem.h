//
//  LanguageDataItem.h
//  AnyLanguage
//
//  Created by Алексей Петров on 15.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "DataItem.h"

@interface LanguageDataItem : DataItem
@property (nonatomic, strong) NSString *languageID; // key id in response
@property (nonatomic, strong) NSString *object;
@property (nonatomic, strong) NSString *nameOfLanguage;
@end
