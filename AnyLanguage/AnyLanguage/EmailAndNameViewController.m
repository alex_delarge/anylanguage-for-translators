    //
//  SingupViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 02.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "EmailAndNameViewController.h"
#import "AppUtils.h"
#import "AppCore.h"
#import "UserDataItem.h"

@interface EmailAndNameViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *fornameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTextField;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UILabel *aboutRules;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UIButton *rulesButton;
@end

@implementation EmailAndNameViewController

- (void)viewDidLoad {
    SAFE_CODE_BEGIN
    [super viewDidLoad];
    _emailTextField.delegate = self;
    _fornameTextfield.delegate = self;
    _lastnameTextField.delegate = self;
    _phoneNumberTextField.delegate = self;
    _usernameTextField.delegate = self;
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica-Light" size:17.0], NSFontAttributeName,nil]];

     self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:255/255 green:45/255 blue:5/255 alpha:1];
    if (self.navigationController == nil) {
        _aboutRules.hidden = YES;
        _rulesButton.hidden = YES;
        [_continueButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
        [_continueButton setImage:[UIImage imageNamed:@"succes button"] forState:UIControlStateNormal];
        _emailTextField.text = [AppCore sharedInstance].user.email;
        _fornameTextfield.text = [AppCore sharedInstance].user.forname;
        _lastnameTextField.text = [AppCore sharedInstance].user.lastname;
        _phoneNumberTextField.text = [AppCore sharedInstance].user.phoneNumber;
        _usernameTextField.text = [AppCore sharedInstance].user.username;

    }

    SAFE_CODE_END
}

- (IBAction)close:(UIBarButtonItem *)sender {
    SAFE_CODE_BEGIN
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.parentViewController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
           } completion:^(BOOL finished) {
               [weakSelf.navigationController.parentViewController dismissViewControllerAnimated:YES completion:nil];
               [[NSNotificationCenter defaultCenter] postNotificationName:@"kUpdateShadow" object:nil];
                         }];
    SAFE_CODE_END
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)tapToHideKeyboard:(UITapGestureRecognizer *)sender {
    SAFE_CODE_BEGIN
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view endEditing:YES];
    SAFE_CODE_END
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    SAFE_CODE_BEGIN
    if (textField == _emailTextField) {
        [_emailTextField resignFirstResponder];
         @synchronized  (self) {
             [_fornameTextfield becomeFirstResponder];
         }
        [AppCore sharedInstance].user.email = _emailTextField.text;
//        return YES;
    }
    
    if (textField == _fornameTextfield) {
        [_fornameTextfield resignFirstResponder];
         @synchronized  (self) {
            [_lastnameTextField becomeFirstResponder];
         }
        [AppCore sharedInstance].user.forname = _fornameTextfield.text;
//        return YES;
    }
    
    if (textField == _lastnameTextField) {
        [_lastnameTextField resignFirstResponder];
         @synchronized  (self) {
             [_usernameTextField becomeFirstResponder];
         }
        [AppCore sharedInstance].user.lastname = _lastnameTextField.text;
//        return YES;
    }
    
    if (textField == _usernameTextField) {
        [_usernameTextField resignFirstResponder];
         @synchronized  (self) {
             [_phoneNumberTextField becomeFirstResponder];
         }
        [AppCore sharedInstance].user.username = _usernameTextField.text;
        return YES;
    }
    
    if (textField == _phoneNumberTextField) {
         @synchronized  (self) {
             [_phoneNumberTextField resignFirstResponder];
         }
        [AppCore sharedInstance].user.phoneNumber = _phoneNumberTextField.text;
    }
//    [textField resignFirstResponder];
    return NO;
    SAFE_CODE_END
    return NO;
}

- (void) confirm {
    [AppCore sharedInstance].user.email = _emailTextField.text;
    [AppCore sharedInstance].user.forname  = _fornameTextfield.text;
    [AppCore sharedInstance].user.lastname = _lastnameTextField.text;
    [AppCore sharedInstance].user.username = _usernameTextField.text;
    [AppCore sharedInstance].user.phoneNumber = _phoneNumberTextField.text;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (self.navigationController == nil) {
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SAFE_CODE_BEGIN
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Ошибка"
                                          message:@"Вы не задали Email"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:cancelAction];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view endEditing:YES];
    if (([_emailTextField.text length] != 0) && ([_fornameTextfield.text length] != 0) && ([_lastnameTextField.text length] != 0) && ([_usernameTextField.text length] != 0) && ([_phoneNumberTextField.text length] != 0)) {
        if ([AppUtils validateEmailWithString:_emailTextField.text]) {
            [AppCore sharedInstance].user.email = _emailTextField.text;
            [AppCore sharedInstance].user.forname  = _fornameTextfield.text;
            [AppCore sharedInstance].user.lastname = _lastnameTextField.text;
            [AppCore sharedInstance].user.username = _usernameTextField.text;
            [AppCore sharedInstance].user.phoneNumber = _phoneNumberTextField.text;
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            [self.view endEditing:YES];
        }
        else {
            alertController.message = @"Вы ввели неправильный email";
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    SAFE_CODE_END
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.navigationController) {
         [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    return YES;
}

- (void) dealloc {
    [self.view endEditing:YES];
    
}

@end
