//
//  FinanseCollectionViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 22.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinanseCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
