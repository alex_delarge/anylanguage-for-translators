//
//  AddTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 14.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "AddTableViewCell.h"

@implementation AddTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [_addButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
}

@end
