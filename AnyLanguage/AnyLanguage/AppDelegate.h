//
//  AppDelegate.h
//  AnyLanguage
//
//  Created by Алексей Петров on 30.07.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)resetWindowToInitialView;
@end

