//
//  TableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "EmailAndNameTableViewCell.h"
#import "AppCore.h"
#import "UserDataItem.h"

@interface EmailAndNameTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *fornameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@end

@implementation EmailAndNameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    [self reloadData];
}

- (void) reloadData {
    _lastnameLabel.text = ([[AppCore sharedInstance].user.lastname length] != 0) ? [AppCore sharedInstance].user.lastname : @"Не указано";
    _fornameLabel.text = ([[AppCore sharedInstance].user.forname length] != 0) ? [AppCore sharedInstance].user.forname : @"Не указано";
    _usernameLabel.text = ([[AppCore sharedInstance].user.username length] != 0) ? [AppCore sharedInstance].user.username : @"Не указано";
    _phoneLabel.text = ([[AppCore sharedInstance].user.phoneNumber length] != 0) ? [AppCore sharedInstance].user.phoneNumber : @"Не указано";
    _emailLabel.text = ([[AppCore sharedInstance].user.email length] != 0) ? [AppCore sharedInstance].user.email : @"Не указано";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
