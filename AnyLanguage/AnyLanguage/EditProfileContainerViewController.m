//
//  EditProfileContainerViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 10.09.15.
//  Copyright © 2015 Алексей Петров. All rights reserved.
//

#import "EditProfileContainerViewController.h"

@interface EditProfileContainerViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@end

@implementation EditProfileContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular) {
        CALayer *vLayer = _containerView.layer;
        [vLayer setCornerRadius:5];
        [vLayer setBorderWidth:0];
        [vLayer setMasksToBounds:YES];
        
        CALayer *viewLayer = _containerView.layer;
        viewLayer.masksToBounds = NO;
        viewLayer.shadowOffset = CGSizeMake(-5, 5);
        viewLayer.shadowRadius = 3;
        viewLayer.shadowRadius = 5;
        viewLayer.shadowOpacity = 0.5;
        
    }// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
