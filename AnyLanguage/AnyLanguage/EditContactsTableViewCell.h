//
//  EditContactsTableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 12.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditContactsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *numberText;
@property (weak, nonatomic) IBOutlet UITextField *vkURLText;
@property (weak, nonatomic) IBOutlet UITextField *fbURLText;
@property (weak, nonatomic) IBOutlet UITextField *liURLText;
@end
