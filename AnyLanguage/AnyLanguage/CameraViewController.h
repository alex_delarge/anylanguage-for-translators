//
//  CameraViewController.h
//  AnyLanguage
//
//  Created by Алексей Петров on 08.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#include <AssetsLibrary/AssetsLibrary.h>

@class CameraViewController;

@protocol CameraViewControllerDelegate  <NSObject>
- (void) cameraViewController:(CameraViewController *)controller didFinishMakingPhoto:(UIImage *)photo;
@end


@interface CameraViewController : UIViewController
@property (nonatomic, weak) id <CameraViewControllerDelegate> delegate;
@end
