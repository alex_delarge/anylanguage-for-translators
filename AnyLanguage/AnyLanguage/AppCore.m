//
//  AppCore.m
//  AnyLanguage
//
//  Created by Алексей Петров on 05.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "AppCore.h"
#import "RequestManager.h"
#import "AppUtils.h"
#import "AFNetworking.h"
#import "Meta.h"
#import "UserDataItem.h"
#import "LanguageDataItem.h"

static  NSString *kAPIBaseURL = @"https://anylanguage.ru:1488/api";
static  NSString *kAppKey = @"secret";

@interface AppCore ()
@property (nonatomic, strong) RequestManager *requestManager;
@property (nonatomic, strong) NSString *token;
@end

@implementation AppCore

+ (AppCore *)sharedInstance {
    static dispatch_once_t pred;
    static AppCore *sharedInstance = nil;
    
    dispatch_once(&pred, ^{ sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _requestManager = [[RequestManager alloc] initWithAPIBaseURL:kAPIBaseURL andAppKey:kAppKey];
        _user = [[UserDataItem alloc] init];
    }
    return self;
}

- (UserDataItem *) createUser {
    _user = [[UserDataItem alloc] init];
    return _user;
}

- (void) requestAPI:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) params succes:(void (^) (id response, AFHTTPRequestOperation *operation)) succes failure: (void (^) (AFHTTPRequestOperation *operation, NSError *error)) failure {
    SAFE_CODE_BEGIN
    __weak typeof(self) weakSelf = self;
    if ([request isEqualToString:@"auth"]) {
        [_requestManager postRequest:request inGroup:group withParameters:params succses:^(id response, AFHTTPRequestOperation *operation) {
            SAFE_CODE_BEGIN
            Meta *meta = response[@"meta"];
            if ([meta.status  isEqual: @200]) {
                if ([response objectForKey:@"token"]) {
                    _token = response[@"token"];
                    succes(meta, operation);
                }
            }
            else {
                NSError *error = [[NSError alloc] initWithDomain:@"" code:[meta.status integerValue] userInfo:[NSDictionary dictionaryWithObject:meta forKey:@"meta"]];
                failure(operation, error);
            }
            SAFE_CODE_END
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            SAFE_CODE_BEGIN
            failure(operation, error);
            SAFE_CODE_END
        }];
    }
    if ([request isEqualToString:@"list"]) {
        [_requestManager getRequest:request inGroup:group withParameters:params succses:^(id response, AFHTTPRequestOperation *operation) {
            SAFE_CODE_BEGIN
            Meta *meta = response[@"meta"];
            if ([meta.status  isEqual: @200]) {
                            if ([response[@"languages"] isKindOfClass:([NSArray class])]) {
                                NSMutableArray *arrayWithLanguages = @[].mutableCopy;
                                for (NSDictionary *dictionary in response[@"languages"]) {
                                   [arrayWithLanguages addObject:[weakSelf parseDicionaryToDataItem:dictionary]];
                                }
                                if (operation) {
                                     succes([NSArray arrayWithArray:arrayWithLanguages], operation);
                                }
                            }
                            else {
                                NSError *error = [[NSError alloc] initWithDomain:@"AppCore" code:[meta.status integerValue] userInfo:[NSDictionary dictionaryWithObject:meta forKey:@"meta"]];
                                failure(operation, error);
                            }
            }
            else {
                NSError *error = [[NSError alloc] initWithDomain:@"" code:[meta.status integerValue] userInfo:[NSDictionary dictionaryWithObject:meta forKey:@"meta"]];
                failure(operation, error);
            }
            SAFE_CODE_END
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            SAFE_CODE_BEGIN
            failure(operation, error);
            SAFE_CODE_END
        }];
    }
    SAFE_CODE_END
}

- (id) parseDicionaryToDataItem:(NSDictionary *) dictionary {
    SAFE_CODE_BEGIN
    if ([dictionary[@"object"] isEqualToString:@"meta"]) {
        return [[Meta alloc] initWithDictionary:dictionary];
    }
    if ([dictionary[@"object"] isEqualToString:@"language"]) {
        return [[LanguageDataItem alloc] initWithDictionary:dictionary];
    }
    return nil;
    SAFE_CODE_END
    return nil;
}

-(void) dealloc {
    _requestManager = nil;
    _token = nil;
}

@end
