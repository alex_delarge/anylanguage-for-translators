//
//  BDataTableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 04.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BDataTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIDatePicker *dataPicker;
@end
