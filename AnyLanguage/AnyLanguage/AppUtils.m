//
//  AppUtils.m
//  AnyLanguage
//
//  Created by Алексей Петров on 30.07.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "AppUtils.h"
#import "INTULocationManager.h"

NSString * const kCachedDateFormatterKey = @"CachedDateFormatterKey";

@interface AppUtils ()

@end

@implementation AppUtils

+ (AppUtils *)sharedInstance {
    static dispatch_once_t pred;
    static AppUtils *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (NSDateFormatter *)dateFormatterYYYYMMdd
{
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateFormatter = [threadDictionary objectForKey:kCachedDateFormatterKey];
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU_POSIX"];
        
        [dateFormatter setLocale:enUSPOSIXLocale];
        [dateFormatter setDateFormat:@"d MMMM YYYYг."];
        
        [threadDictionary setObject:dateFormatter forKey:kCachedDateFormatterKey];
    }
    return dateFormatter;
}

+ (BOOL) isIPhone {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        return YES;
    }
    return NO;
}

+ (BOOL) isIPad {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        return YES;
    }
    return NO;
}

+ (void) getLocationWithCompletetion:(void (^)(NSDictionary *geolocation)) completetion  {
    SAFE_CODE_BEGIN
//    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    INTULocationAccuracy accuracy = INTULocationAccuracyCity;
    [locMgr requestLocationWithDesiredAccuracy:accuracy timeout:10.0 delayUntilAuthorized:YES block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        SAFE_CODE_BEGIN
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        double lati = currentLocation.coordinate.latitude; //45.46433;
        double longi = currentLocation.coordinate.longitude; //9.18839;
        CLLocation *location = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(lati, longi)
                                                             altitude:0
                                                   horizontalAccuracy:0
                                                     verticalAccuracy:0
                                                            timestamp:[NSDate date]];
        [geocoder reverseGeocodeLocation:location completionHandler:
         ^(NSArray *placemarks, NSError *error)
         {
             SAFE_CODE_BEGIN
             CLPlacemark *placemark = [placemarks lastObject];
             if (error || !placemark) {
                 return;
             }
             NSString *city = placemark.locality;
             NSString *country = placemark.country;
             if (!city) {
                 city = placemark.subAdministrativeArea;
             }
             NSDictionary *geolocation = @{}.copy;
             if (city && country) {
                 geolocation = @{@"city" : city, @"country" : country};
                 completetion(geolocation);
            }
             else {
                 completetion (nil);
             }
             SAFE_CODE_END
         }];
        SAFE_CODE_END
        }];
    SAFE_CODE_END
   }

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



@end
