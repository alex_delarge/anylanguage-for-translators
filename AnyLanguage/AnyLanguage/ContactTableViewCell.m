//
//  ContactTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 12.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "ContactTableViewCell.h"

@implementation ContactTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) prepareForReuse {
    _iconImageView.image = nil;
    _nameLabel.text = @"";
}

@end
