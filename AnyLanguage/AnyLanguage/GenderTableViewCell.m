//
//  gender gender gender GenderTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 07.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "GenderTableViewCell.h"
#import "GenderCollectionViewCell.h"

@interface GenderTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation GenderTableViewCell

- (void)awakeFromNib {
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
     [_collectionView registerNib:[UINib nibWithNibName:@"GenderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GenderCollectionViewCell"];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GenderCollectionViewCell *genderCollectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GenderCollectionViewCell" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        genderCollectionViewCell.genderName.text = @"Мужской";
    }
    else if (indexPath.row == 1) {
        genderCollectionViewCell.genderName.text = @"Женский";
    }
    return genderCollectionViewCell;
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    GenderCollectionViewCell *genderCollectionViewCell = (GenderCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [genderCollectionViewCell.confirmImageView setHidden:YES];
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GenderCollectionViewCell *genderCollectionViewCell = (GenderCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [genderCollectionViewCell.confirmImageView setHidden:NO];
}

@end
