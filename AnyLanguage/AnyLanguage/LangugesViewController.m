//
//  LangugesViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 11.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "LangugesViewController.h"
#import "LanguagesTableViewCell.h"
#import "LanguageDataItem.h"
#import "UserDataItem.h"
#import "ProgressHUD.h"
#include "AppUtils.h"
#import "AppCore.h"

@interface LangugesViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (strong, nonatomic) NSArray *sourseArray;
@property (strong, nonatomic) NSMutableArray *outputArray;
@end

@implementation LangugesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.hidden = YES;
    [_tableView registerNib:[UINib nibWithNibName:@"LanguagesTableViewCell" bundle:nil] forCellReuseIdentifier:@"LanguagesTableViewCell"];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _outputArray = @[].mutableCopy;
    if ([AppCore sharedInstance].user.languages.count > 0) {
        _outputArray = (NSMutableArray *)[AppCore sharedInstance].user.languages;
    }
    _sourseArray = @[].copy;
      if (self.navigationController == nil) {
        [_continueButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
        [_continueButton setImage:[UIImage imageNamed:@"succes button"] forState:UIControlStateNormal];
    }
}

- (IBAction)close:(UIBarButtonItem *)sender {
    SAFE_CODE_BEGIN
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.parentViewController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
    } completion:^(BOOL finished) {
        [weakSelf.navigationController.parentViewController dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kUpdateShadow" object:nil];
    }];
    SAFE_CODE_END
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = self.view.center;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = @{}.mutableCopy;
    __weak typeof(self) weakSelf = self;
    [[AppCore sharedInstance] requestAPI:@"list" inGroup:@"baseLanguage" withParameters:params succes:^(id response, AFHTTPRequestOperation *operation) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        if (response) {
            weakSelf.sourseArray = response;
            [UIView animateWithDuration:0.6 animations:^{
                weakSelf.tableView.hidden = NO;
                [weakSelf.tableView reloadData];
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {}];
    
}

- (void) confirm {
    [AppCore sharedInstance].user.languages = _outputArray;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _sourseArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LanguagesTableViewCell *languagesCell = [tableView dequeueReusableCellWithIdentifier:@"LanguagesTableViewCell" forIndexPath:indexPath];
    LanguageDataItem *language = [_sourseArray objectAtIndex:indexPath.row];
    [languagesCell setNameOfLanguage:language.nameOfLanguage];
    NSMutableArray *temporateArray = @[].mutableCopy;
    for (LanguageDataItem *lang in  _outputArray) {
        if ([language.languageID isEqualToString:lang.languageID]) {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            [temporateArray addObject:language];
        }
    }
    return languagesCell;
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    LanguagesTableViewCell *selectedCell = (LanguagesTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    LanguageDataItem *language = [_sourseArray objectAtIndex:indexPath.row];
    LanguageDataItem *removeLanguage = nil;
    for (LanguageDataItem *lang in  _outputArray) {
        if ([language.languageID isEqualToString:lang.languageID]) {
            removeLanguage = lang;
        }
    }

    [_outputArray removeObject:removeLanguage];
    
    [selectedCell select];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LanguagesTableViewCell *selectedCell = (LanguagesTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [_outputArray addObject:[_sourseArray objectAtIndex:indexPath.row]];
    [selectedCell select];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
        LanguagesTableViewCell *languageCell = (LanguagesTableViewCell *)cell;
        if (languageCell.selected) {
            [languageCell select];
        }  
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (self.navigationController == nil) {
        return NO;
    }
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [AppCore sharedInstance].user.languages = _outputArray;
}


@end
