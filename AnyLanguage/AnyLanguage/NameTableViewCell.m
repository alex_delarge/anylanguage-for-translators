//
//  TableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 04.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "NameTableViewCell.h"
#import "AppCore.h"
#import "UserDataItem.h"

@implementation NameTableViewCell

- (void)awakeFromNib {
    if (![[AppCore sharedInstance].user.forname isEqualToString:@""]) {
        _fornameText.text = [AppCore sharedInstance].user.forname;
    }
    
    if (![[AppCore sharedInstance].user.lastname isEqualToString:@""]) {
        _lastnameTexField.text = [AppCore sharedInstance].user.lastname;
    }
    
    if (![[AppCore sharedInstance].user.username isEqualToString:@""]) {
        _usernameText.text = [AppCore sharedInstance].user.username;
    }
}

- (void) prepareForReuse {
    [_usernameText resignFirstResponder];
    [_fornameText resignFirstResponder];
}

@end
