//
//  Meta.h
//  AnyLanguage
//
//  Created by Алексей Петров on 06.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "DataItem.h"

@interface Meta : DataItem
@property (nonatomic, strong) NSString *object;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSArray *errorMessages;
- (instancetype) initWithDictionary:(NSDictionary *)resourseDictionary;
@end
