//
//  DataManager.m
//  Anylanguage
//
//  Created by Алексей Петров on 02.05.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "RequestManager.h"
#import "AFNetworking.h"
#import "AppCore.h"
#import "AppUtils.h"
#import "Meta.h"

@interface RequestManager ()
@property (nonatomic, strong) NSString *accesToken;
@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) NSString *appKey;
@end

@implementation RequestManager


- (instancetype) initWithAPIBaseURL:(NSString *) url andAppKey:(NSString *) appKey
{
    SAFE_CODE_BEGIN
    self = [super init];
    if (self) {
        if (([url isKindOfClass:([NSString class])]) && ([appKey isKindOfClass:([NSString class])])) {
            _baseURL = url;
            _appKey = appKey;
        }
    }
    return self;
    SAFE_CODE_END
    return nil;
}

- (void) postRequest:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) parameters succses: (void (^)(id response, AFHTTPRequestOperation *operation)) succes failure: (void (^)(AFHTTPRequestOperation *operation,  NSError *error)) failure
 {
    SAFE_CODE_BEGIN
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [parameters setObject:_appKey forKey:@"app_key"];
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@",_baseURL, group, request];
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_CODE_BEGIN
        if ([responseObject isKindOfClass:([NSDictionary class])]) {
            if (responseObject[@"meta"]) {
                NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObject];
                [returnDictionary setObject:[[AppCore sharedInstance] parseDicionaryToDataItem:responseObject[@"meta"]] forKey:@"meta"];
                succes(returnDictionary, operation);
            }
            else {
                NSError * error;
                failure(operation, error);
            }
        }
        else {
            NSError * error = nil;
            failure(operation, error);
        }
        SAFE_CODE_END
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_CODE_BEGIN
        failure(operation, error);
        SAFE_CODE_END
    }];
    SAFE_CODE_END
}

- (void) getRequest:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) parameters succses: (void (^)(id response, AFHTTPRequestOperation *operation)) succes failure: (void (^)(AFHTTPRequestOperation *operation,  NSError *error)) failure {
    SAFE_CODE_BEGIN
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [parameters setObject:_appKey forKey:@"app_key"];
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@",_baseURL, group, request];
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        SAFE_CODE_BEGIN
        if ([responseObject isKindOfClass:([NSDictionary class])]) {
            if (responseObject[@"meta"]) {
                NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObject];
                [returnDictionary setObject:[[AppCore sharedInstance] parseDicionaryToDataItem:responseObject[@"meta"]] forKey:@"meta"];
                succes(returnDictionary, operation);
            }
            else {
                NSError *error ;
                failure(operation, error);
            }
        }
        else {
            NSError * error = nil;
            failure(operation, error);
        }
        SAFE_CODE_END
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        SAFE_CODE_BEGIN
        Meta *meta = [[Meta alloc] init];
//        meta.status = opera;
//        NSError *returnedError = [NSError alloc] initWithDomain:<#(NSString *)#> code:<#(NSInteger)#> userInfo:<#(NSDictionary *)#>
        failure(operation, error);
        SAFE_CODE_END
    }];
    SAFE_CODE_END

    
}

@end