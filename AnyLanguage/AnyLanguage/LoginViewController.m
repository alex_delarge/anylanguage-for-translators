//
//  LoginViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 01.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "LoginViewController.h"
#import "AppUtils.h"
#import "AppCore.h"
#import "PDKeychainBindings.h"
#import "ProgressHUD.h"
#import "AFHTTPRequestOperation.h"
#import "Meta.h"
#import "AppDelegate.h"

@interface LoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *showPasswordButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *backgrondView;
@property (weak, nonatomic) UITraitCollection *collectionOfTrains;
@end

static const NSInteger kCornerRadius = 5;

@implementation LoginViewController

- (IBAction)showPassword:(UIButton *)sender {
    SAFE_CODE_BEGIN
    _passwordTextField.secureTextEntry = !_passwordTextField.secureTextEntry;
    sender.selected = !sender.selected;
    SAFE_CODE_END
}


- (void)viewDidLoad {
    SAFE_CODE_BEGIN
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeWithNotification) name:@"kFinishNatification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateShadow) name:@"kUpdateShadow" object:nil];
     self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:255/255 green:45/255 blue:5/255 alpha:1];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica-Light" size:17.0], NSFontAttributeName,nil]];
    
    CALayer *viewLayer = _backgrondView.layer;
    [viewLayer setCornerRadius:kCornerRadius];
    [viewLayer setBorderWidth:0];
    [viewLayer setMasksToBounds:YES];
//    _emailTextField.delegate = self;
//    _passwordTextField.delegate = self;
    _showPasswordButton.selected = NO;
    _showPasswordButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _collectionOfTrains = self.traitCollection;
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular) {
        viewLayer.shadowOffset = CGSizeMake(-5, 5);
        viewLayer.shadowRadius = kCornerRadius;
        viewLayer.shadowOpacity = 0.5;
        viewLayer.masksToBounds = NO;
    }
    SAFE_CODE_END
}

- (void) updateShadow {
    CALayer *viewLayer = _backgrondView.layer;
    if (_collectionOfTrains.horizontalSizeClass == UIUserInterfaceSizeClassRegular) {
        viewLayer.shadowOffset = CGSizeMake(-5, 5);
        viewLayer.shadowRadius = kCornerRadius;
        viewLayer.shadowOpacity = 0.5;
        viewLayer.masksToBounds = NO;
    }
    

}

- (void) traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    if (previousTraitCollection == nil) {
        return;
    }
    if (previousTraitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact) {
        [UIView animateWithDuration:0.3 animations:^{
            _backgrondView.layer.masksToBounds = NO;
            _backgrondView.layer.shadowOffset = CGSizeMake(-5, 5);
            _backgrondView.layer.shadowRadius = kCornerRadius;
            _backgrondView.layer.shadowOpacity = 0.5;
        }];
    } else {
        _backgrondView.layer.masksToBounds = YES;
        _backgrondView.layer.cornerRadius = 0;
        _backgrondView.layer.shadowOffset = CGSizeMake(0, 0);
        _backgrondView.layer.shadowRadius = 0;
        _backgrondView.layer.shadowOpacity = 0;
    }
}

-(void) closeWithNotification {
    SAFE_CODE_BEGIN
    [self dismissViewControllerAnimated:YES completion:nil];
    SAFE_CODE_END
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)login:(UIButton *)sender {
    SAFE_CODE_BEGIN
    [self.navigationController resignFirstResponder];
    [self.view resignFirstResponder];
    [self.backgrondView endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    SAFE_CODE_END
}

- (void) loginRequest {
    SAFE_CODE_BEGIN
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Ошибка"
                                          message:@"Неправильно задан Email"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:cancelAction];

    if (([_emailTextField.text length] != 0) && ([_passwordTextField.text length] != 0)) {
        if ([AppUtils validateEmailWithString:_emailTextField.text]) {
            NSMutableDictionary *params = @{}.mutableCopy;
            [params setObject:_emailTextField.text forKey:@"email"];
            [params setObject:_passwordTextField.text forKey:@"password"];
            [ProgressHUD show:nil];
            [[AppCore sharedInstance] requestAPI:@"auth" inGroup:@"user" withParameters:params succes:^(id response, AFHTTPRequestOperation *operation) {
                [ProgressHUD showSuccess:nil];
                [ProgressHUD dismiss];
                [[PDKeychainBindings sharedKeychainBindings] setObject:_emailTextField.text forKey:@"email"];
                [[PDKeychainBindings sharedKeychainBindings] setObject:_passwordTextField.text forKey:@"password"];
                [self.navigationController setNavigationBarHidden:NO animated:YES];
                [self.view endEditing:YES];
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [ProgressHUD showError:nil];
                [ProgressHUD dismiss];
                Meta *meta = [error.userInfo objectForKey:@"meta"];
                alertController.message = [meta.errorMessages firstObject];
                [self presentViewController:alertController animated:YES completion:nil];
            }];
        }
        else {
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }
    SAFE_CODE_END
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    SAFE_CODE_BEGIN
    if (textField == _emailTextField) {
        [_emailTextField resignFirstResponder];
        [_passwordTextField becomeFirstResponder];
    }
    
    if (textField == _passwordTextField) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self loginRequest];
        [_passwordTextField resignFirstResponder];
    }
    
//    [textField resignFirstResponder];
    return NO;
    SAFE_CODE_END
    return NO;
}

- (IBAction)tapToHideKeyboard:(UITapGestureRecognizer *)sender {
    SAFE_CODE_BEGIN
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view endEditing:YES];
    SAFE_CODE_END
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"registration"]) {
        [[AppCore sharedInstance] createUser];
        [UIView animateWithDuration:0.4 animations:^{
            _backgrondView.layer.masksToBounds = NO; 
            _backgrondView.layer.shadowOffset = CGSizeMake(0, 0);
            _backgrondView.layer.shadowRadius = kCornerRadius;
            _backgrondView.layer.shadowOpacity = 0;
        }];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{

}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kFinishNatification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kUpdateShadow" object:nil];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController resignFirstResponder];
    [self.view endEditing:YES];
//    [_emailTextField resignFirstResponder];
//    _emailTextField.delegate = nil;
//    [_passwordTextField resignFirstResponder];
//    _passwordTextField.delegate = nil;
    self.navigationController.hidesBarsWhenKeyboardAppears = NO;
    _emailTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    _passwordTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    _passwordTextField.keyboardType = UIKeyboardTypeDefault;
    _emailTextField.keyboardType = UIKeyboardTypeDefault;
}

@end
