//
//  BDataTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 04.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "BDataTableViewCell.h"
#import "AppUtils.h"

@interface BDataTableViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hightOfDataPicker;
@property (weak, nonatomic) IBOutlet UILabel *bdataLabel;

@end

@implementation BDataTableViewCell

- (void)awakeFromNib {
    NSLocale *ruUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU_POSIX"];
    _dataPicker.locale = ruUSPOSIXLocale;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)showDataPicker:(UIButton *)sender {
    _bdataLabel.text = [[AppUtils dateFormatterYYYYMMdd] stringFromDate:_dataPicker.date]; // [NSString stringWithFormat:@"%@", _dataPicker.date];  // _dataPicker.date;
}

@end
