//
//  AppCore.h
//  AnyLanguage
//
//  Created by Алексей Петров on 05.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestOperation;
@class UserDataItem;

@interface AppCore : NSObject
@property (nonatomic, strong) UserDataItem *user;

+ (AppCore *)sharedInstance;
- (void) requestAPI:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) params succes:(void (^) (id response, AFHTTPRequestOperation *operation)) succes failure: (void (^) (AFHTTPRequestOperation *operation, NSError *error)) failure;
- (id) parseDicionaryToDataItem:(NSDictionary *) dictionary;
- (UserDataItem *) createUser;
@end
