//
//  AppUtils.h
//  AnyLanguage
//
//  Created by Алексей Петров on 30.07.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


#define SAFE_CODE_BEGIN @try {
#define SAFE_CODE_END \
} \
@catch (NSException *exception) { \
    NSLog(@"trouth exception %@", exception.description); \
}

@interface AppUtils : NSObject
+ (AppUtils *)sharedInstance;

/**
    Device checkers
 */
+ (NSDateFormatter *)dateFormatterYYYYMMdd;

+ (BOOL) isIPhone;
+ (BOOL) isIPad;
+ (void) getLocationWithCompletetion:(void (^)(NSDictionary *geolocation)) completetion;
+ (BOOL)validateEmailWithString:(NSString*)email;

@end
