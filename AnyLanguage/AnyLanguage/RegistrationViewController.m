//
//  LocationViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 11.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "RegistrationViewController.h"
#import "LangugesViewController.h"
#import "AppUtils.h"
#import "AppCore.h"
#import "UserDataItem.h"
#import "LanguageDataItem.h"
#import "UserPicTableViewCell.h"
#import "RegistrationHeaderView.h"
#import "EmailAndNameTableViewCell.h"
#import "LocationTableViewCell.h"
#import "LanguagesTableViewCell.h"
#import "AddTableViewCell.h"
#import "CameraViewController.h"
#import "EmailAndNameViewController.h"

static const NSInteger kSectionWithUserImage = 0; // with user pic
static const NSInteger kSectionWithUserInfo = 1;
static const NSInteger kSectionWithUserLocation = 2;
static const NSInteger kSectionWithUserLanguages = 3;
static const NSString *kFinishNatification = @"kFinishNatification";

@interface RegistrationViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *sourseArray;
@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//    [self.navigationController.navigationBar setTranslucent:YES];
    _sourseArray = @[].copy;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 44;
    _tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    [_tableView registerNib:[UINib nibWithNibName:@"UserPicTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserPicTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"EmailAndNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmailAndNameTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"LocationTableViewCell" bundle:nil] forCellReuseIdentifier:@"LocationTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"LanguagesTableViewCell" bundle:nil] forCellReuseIdentifier:@"LanguagesTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"AddTableViewCell" bundle:nil] forCellReuseIdentifier:@"AddTableViewCell"];
}

- (IBAction)close:(UIBarButtonItem *)sender {
    SAFE_CODE_BEGIN
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.parentViewController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
    } completion:^(BOOL finished) {
        [weakSelf.navigationController.parentViewController dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kUpdateShadow" object:nil];
    }];
    SAFE_CODE_END
}

#pragma mark - UITableViewDelegate

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _sourseArray = [AppCore sharedInstance].user.languages;
    [_tableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == kSectionWithUserLanguages) {
        return _sourseArray.count + 1;
    }
    if (section == kSectionWithUserInfo) {
        return 2;
    }
    if (section == kSectionWithUserLocation) {
        return 2;
    }
    return 2;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == kSectionWithUserImage) {
        return CGFLOAT_MIN;
    }
    return 40;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
        RegistrationHeaderView *registrationHeaderView = [[RegistrationHeaderView alloc] init];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RegistrationHeaderView" owner:self options:nil];
    registrationHeaderView = (RegistrationHeaderView *)[nib objectAtIndex:0];
    switch (section) {
        case kSectionWithUserImage:
            return nil;
            break;
        case kSectionWithUserInfo: {
            [registrationHeaderView configHeaderWithText:@"Данные"];
            return registrationHeaderView;
            break;
        }
        case kSectionWithUserLocation: {
            [registrationHeaderView configHeaderWithText:@"Местоположение"];
            return registrationHeaderView;
            break;
        }
        case kSectionWithUserLanguages: {
            [registrationHeaderView configHeaderWithText:@"Языки"];
            return registrationHeaderView;
        }
        default:
            return nil;
            break;
    }
    return nil;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSectionWithUserImage) {
        if (indexPath.row == 0) {
            UserPicTableViewCell *userPicTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UserPicTableViewCell" forIndexPath:indexPath];
            [userPicTableViewCell reloadData];
            return userPicTableViewCell;
        }
        else if (indexPath.row == 1) {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [addTAbleViewCell.addButton addTarget:self action:@selector(changeUserPicture) forControlEvents:UIControlEventTouchUpInside];
            [addTAbleViewCell.addButton setTitle:@"Изменить" forState:UIControlStateNormal];
            return addTAbleViewCell;
        }
    }
    if (indexPath.section == kSectionWithUserInfo) {
        if (indexPath.row == 0) {
            EmailAndNameTableViewCell *emailAndNameTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"EmailAndNameTableViewCell" forIndexPath:indexPath];
            [emailAndNameTableViewCell reloadData];
            return emailAndNameTableViewCell;
        }
        else if (indexPath.row == 1) {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [addTAbleViewCell.addButton addTarget:self action:@selector(changeUserData) forControlEvents:UIControlEventTouchUpInside];
            [addTAbleViewCell.addButton setTitle:@"Изменить" forState:UIControlStateNormal];
            return addTAbleViewCell;
        }
    }
    if (indexPath.section == kSectionWithUserLocation) {
        if (indexPath.row == 0) {
            LocationTableViewCell *locationTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"LocationTableViewCell" forIndexPath:indexPath];
            [locationTableViewCell reloadData];
            return locationTableViewCell;
        } else if (indexPath.row == 1) {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [ addTAbleViewCell.addButton setTitle:@"Изменить" forState:UIControlStateNormal];
            [addTAbleViewCell.addButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
            return addTAbleViewCell;
        }
        
    }
    if (indexPath.section == kSectionWithUserLanguages) {
        if (indexPath.row < _sourseArray.count) {
            LanguagesTableViewCell *languagesTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"LanguagesTableViewCell" forIndexPath:indexPath];
            LanguageDataItem *language = _sourseArray[indexPath.row];
            [languagesTableViewCell setNameOfLanguage:language.nameOfLanguage];
            return languagesTableViewCell;
        } else {
            AddTableViewCell *addTAbleViewCell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
            [addTAbleViewCell.addButton addTarget:self action:@selector(addNewLanguages) forControlEvents:UIControlEventTouchUpInside];
            [ addTAbleViewCell.addButton setTitle:@"Добавить" forState:UIControlStateNormal];
            return addTAbleViewCell;
        }
    }
    UITableViewCell *tableViewCell =  [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    return tableViewCell;
}

- (void) addNewLanguages {
    LangugesViewController *languagesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LangugesViewController"];
    [self.navigationController presentViewController:languagesViewController animated:YES completion:nil];
}

- (void) changeUserLocation {
    UINavigationController *navigationWithSettingUserLocation = [self.storyboard instantiateViewControllerWithIdentifier:@"setLocation"];
    navigationWithSettingUserLocation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    navigationWithSettingUserLocation.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:navigationWithSettingUserLocation animated:YES completion:nil];
}

- (void) changeUserPicture {
    UINavigationController *navigationWithCameraViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    navigationWithCameraViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navigationWithCameraViewController animated:YES completion:nil];
}

- (void) changeUserData {
    EmailAndNameViewController *emailAndNameViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EmailAndNameViewController"];
    [self presentViewController:emailAndNameViewController animated:YES completion:nil];
}

- (IBAction)sendRegistrationRequest:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.navigationController.parentViewController.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
    } completion:^(BOOL finished) {
        [weakSelf.navigationController.parentViewController dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kFinishNatification object:nil];
        }];
    
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
