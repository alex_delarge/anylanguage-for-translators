//
//  AddTableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 14.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@end
