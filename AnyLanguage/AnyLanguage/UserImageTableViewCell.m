//
//  UserImageTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 03.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "UserImageTableViewCell.h"
#import "AppCore.h"
#import "UserDataItem.h"

@implementation UserImageTableViewCell

- (void)awakeFromNib {
    CALayer *imageLayer = _userPicImageView.layer;
    [imageLayer setCornerRadius:_userPicImageView.frame.size.height/2];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
}

- (void) reloadData {
    if ([AppCore sharedInstance].user.userPicture) {
        _userPicImageView.image = [AppCore sharedInstance].user.userPicture;
    }
}

@end
