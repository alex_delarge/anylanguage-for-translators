//
//  Meta.m
//  AnyLanguage
//
//  Created by Алексей Петров on 06.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "Meta.h"

@implementation Meta

- (instancetype) initWithDictionary:(NSDictionary *)resourseDictionary {
    self = [super initWithDictionary:resourseDictionary];
    if (self) {
        _object = resourseDictionary[@"object"];    
        _status = resourseDictionary[@"status"];
        _errorMessages = [NSArray arrayWithArray:resourseDictionary[@"error_messages"]];
    }
    return self;
}

@end
