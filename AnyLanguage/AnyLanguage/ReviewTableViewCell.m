//
//  ReviewTableViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 14.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "ReviewTableViewCell.h"
#import "CommentCollectionViewCell.h"

@interface ReviewTableViewCell () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerNib:[UINib nibWithNibName:@"CommentCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CommentCollectionViewCell"];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CommentCollectionViewCell *commentCollectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CommentCollectionViewCell" forIndexPath:indexPath];
    return commentCollectionViewCell;
}

@end
