//
//  CommentCollectionViewCell.m
//  AnyLanguage
//
//  Created by Алексей Петров on 14.09.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "CommentCollectionViewCell.h"

@interface CommentCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIView *cellBackgroundView;
@end

@implementation CommentCollectionViewCell

- (void)awakeFromNib {
    
    CALayer *vLayer = _userPic.layer;
    [vLayer setCornerRadius:75.0/2];
    [vLayer setBorderWidth:1];
    [vLayer setBorderColor:[[UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:255.0/255.0] CGColor]];
    [vLayer setMasksToBounds:YES];
    
    }

@end
