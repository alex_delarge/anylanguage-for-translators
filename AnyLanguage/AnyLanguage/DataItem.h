//
//  DataItem.h
//  AnyLanguage
//
//  Created by Алексей Петров on 06.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataItem : NSObject
- (instancetype) initWithDictionary:(NSDictionary *) resourseDictionary;
@end
