//
//  TaskListViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 19.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "TaskListViewController.h"
#import "AppDelegate.h"
#import "TestViewController.h"
#import "LoginViewController.h"

@interface TaskListViewController ()// <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *emptyState;
@property (weak, nonatomic) IBOutlet UITextField *text;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UISegmentedControl *filterSegment;
@end

@implementation TaskListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyStateView" owner:self options:nil];
    UIView *emptyStateView = [[UIView alloc] init];
    emptyStateView = (UIView *)[nib objectAtIndex:0];
    emptyStateView.frame = CGRectMake(0, 0, _backgroundImage.frame.size.width, _backgroundImage.frame.size.height + 64);
    [_backgroundImage addSubview:emptyStateView];
    [_filterSegment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Helvetica-Light" size:15.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
