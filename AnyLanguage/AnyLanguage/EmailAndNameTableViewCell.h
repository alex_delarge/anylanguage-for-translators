//
//  TableViewCell.h
//  AnyLanguage
//
//  Created by Алексей Петров on 13.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailAndNameTableViewCell : UITableViewCell
- (void) reloadData;
@end
