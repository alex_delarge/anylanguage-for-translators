//
//  SetPasswordViewController.m
//  AnyLanguage
//
//  Created by Алексей Петров on 03.08.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import "SetPasswordViewController.h"

@interface SetPasswordViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIView *succesView;
@end

@implementation SetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _passwordTextField.delegate = self;
    _confirmPasswordTextField.delegate = self;
}

- (IBAction)succes:(UIButton *)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Ошибка"
                                          message:@"Пароли не совпадают"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:cancelAction];
    if ([_passwordTextField.text length] > 0) {
        if ([_passwordTextField.text isEqualToString:_confirmPasswordTextField.text]) {
            
            [self.view endEditing:YES];
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            _succesView.hidden = NO;
            NSTimeInterval animationDuration = 0.60;
            CGRect newFrameSize = self.view.frame;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            _succesView.frame = newFrameSize;
            [UIView commitAnimations];
            
        }
        else {
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else {
        alertController.message = @"Вы не ввели пароль";
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)tapToHideKeyboard:(UITapGestureRecognizer *)sender {
    if (_succesView.hidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.view endEditing:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
