//
//  DataManager.h
//  Anylanguage
//
//  Created by Алексей Петров on 02.05.15.
//  Copyright (c) 2015 Алексей Петров. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AFHTTPRequestOperation;

@interface RequestManager : NSObject
// init
- (instancetype) initWithAPIBaseURL:(NSString *) url andAppKey:(NSString *) appKey;

- (void) postRequest:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) parameters succses: (void (^)(id response, AFHTTPRequestOperation *operation)) succes failure: (void (^)(AFHTTPRequestOperation *operation,  NSError *error)) failure;
- (void) getRequest:(NSString *) request inGroup:(NSString *) group withParameters:(NSMutableDictionary *) parameters succses: (void (^)(id response, AFHTTPRequestOperation *operation)) succes failure: (void (^)(AFHTTPRequestOperation *operation,  NSError *error)) failure;
@end
